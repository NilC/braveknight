﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip SmashSound;
    public AudioClip PickUpSound;
    public AudioClip JumpSound;
    public AudioClip ClampSound;
    public AudioClip WeeSound;

    public void PlaySmashSound(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(SmashSound, position);
    }

    public void PlayPickUpSound(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(PickUpSound, position);
    }

    public void PlayJumpSound(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(JumpSound, position);
    }

    public void PlayClampSound(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(ClampSound, position);
    }

    public void PlayWeeSound(Vector3 position)
    {
        AudioSource.PlayClipAtPoint(WeeSound, position);
    }
}

