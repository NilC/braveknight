﻿using UnityEngine;
using UnityEngine.UI;

public class DependencesInjector : MonoBehaviour
{
    public Hero Hero;
    public TerrainGenerator TerrainGenerator;
    public Score Score;
    public PrincessSpawner PrincessSpawner;
    public Animator GameOverMenuAnimator;
    public AudioManager AudioManager;
    public Text DebugLabel;

    private void Awake()
    {
        Dependences.Hero = Hero;
        Dependences.TerrainGenerator = TerrainGenerator;
        Dependences.Score = Score;
        Dependences.PrincessSpawner = PrincessSpawner;
        Dependences.GameOverMenuAnimator = GameOverMenuAnimator;
        Dependences.AudioManager = AudioManager;
        Dependences.DebugLabel = DebugLabel;
    }
}
