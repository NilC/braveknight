﻿using UnityEngine;
using UnityEngine.UI;

public class Dependences
{
    public static Hero Hero;
    public static TerrainGenerator TerrainGenerator;
    public static Score Score;
    public static PrincessSpawner PrincessSpawner;
    public static Animator GameOverMenuAnimator;
    public static AudioManager AudioManager;
    public static Text DebugLabel;
}

