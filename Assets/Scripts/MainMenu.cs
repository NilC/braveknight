﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{
    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    void Update()
    {
        if (InputHelper.IsTapped())
        {
            Application.LoadLevel("Main");
        }
    }
}
