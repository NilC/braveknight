﻿using System;
using UnityEngine;

public class Score : MonoBehaviour
{
    public int ScoreAmmount;
    public int DepthScore;

    private float heroStartY;

    private void Start()
    {
        ScoreAmmount = 0;
        heroStartY = Dependences.Hero.transform.position.y;
    }

    private void Update()
    {
        if (!Dependences.Hero.IsDead())
        {
            DepthScore = Mathf.RoundToInt(Mathf.Abs(Dependences.Hero.transform.position.y - heroStartY) / 5.0f);
        }        
    }
}
