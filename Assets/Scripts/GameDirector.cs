﻿using System.Collections;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
    private bool isGameOvered;
    private bool isWaited;

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }
 
    private void Update()
    {
        if (!isGameOvered && Dependences.Hero.IsDead())
        {
            StartCoroutine(GameOverCoroutine());
            isGameOvered = true;
        }

        if (isGameOvered && isWaited)
        {
            if (InputHelper.IsTapped())
            {
                Restart();
            }
        }

    }

    public void Restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    private IEnumerator GameOverCoroutine()
    {
        yield return new WaitForSeconds(2.0f);
        isWaited = true;
        Dependences.GameOverMenuAnimator.Play(Triggers.GameOver);
        yield return null;
    }
}
