﻿using UnityEngine;
using System.Collections;

public class Landscape : MonoBehaviour
{
    public float MovingSpeed = 1.0f;

    public void Start()
    {

    }

    public void Update()
    {
        transform.Translate(0.0f, MovingSpeed * Time.deltaTime, 0.0f);
    }
}
