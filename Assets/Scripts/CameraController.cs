﻿using UnityEngine;


public class CameraController : MonoBehaviour
{
    public float Factor = 0.1f;
    public Rigidbody2D TargetTransform;

    public float CurrentTimeFollowAfterDeath = 1.0f;
    private float timeFollowAfterDeath;

    private void FixedUpdate()
    {
        if (Dependences.Hero.IsDead())
        {
            timeFollowAfterDeath += Time.deltaTime;
            if (timeFollowAfterDeath > CurrentTimeFollowAfterDeath)
            {
                Factor = 0.0f;    
            }            
        }

        float delta = TargetTransform.position.y - transform.position.y;
        rigidbody2D.velocity = new Vector2(0, delta * Factor);
    }
}
