﻿public static class Triggers
{
    public const string Clamp = "Clamp";
    public const string Jump = "Jump";
    public const string PreFalling = "PreFalling";
    public const string Falling = "Falling";
    public const string Smash = "Smash";
    public const string GameOver = "GameOver";
}
