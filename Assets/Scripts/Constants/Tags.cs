﻿public static class Tags
{
    public const string MainCamera = "MainCamera";
    public const string Wall = "Wall";
    public const string Creator = "Creator";
    public const string Terminator = "Terminator";    
    public const string DebugLabel = "DebugLabel";
}
