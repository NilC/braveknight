﻿using UnityEngine;

public class PrincessSpawner : MonoBehaviour
{
    public Transform Spawn;
    public float Probality = 0.002f;
    public GameObject Princess;
    public int Ammount { private set; get; }

    private void Update()
    {
        if (Random.Range(0.0f, 1.0f) < Probality)
        {
            Dependences.AudioManager.PlayWeeSound(transform.position);
            Instantiate(Princess, new Vector3(Spawn.position.x, Spawn.position.y, 1.0f), Quaternion.identity);
            Ammount++;
        }
    }
}
