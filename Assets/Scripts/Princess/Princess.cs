﻿using UnityEngine;
using System.Collections;

public class Princess : MonoBehaviour {
    public float TimeToLive = 1.0f;
    private Vector2 Velocity;
    private float bornTime;
	// Use this for initialization
	void Start () {
        var r = Random.Range(-8, 8f);
        Velocity = new Vector2(r, -10);
        bornTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        rigidbody2D.velocity = Velocity; 

        if (Time.time - bornTime > TimeToLive) {
            Destroy(gameObject);
        }
	}

    private void OnTriggerEnter2D(Collider2D other)
    {

        //if (other.gameObject.name == "HeroRoot")
        //{
        //    Destroy(gameObject);
        //}
        Velocity = new Vector2(-Velocity.x, Velocity.y);
	}
}
