﻿using UnityEngine;

public class InputHelper : MonoBehaviour
{
#if (UNITY_ANDROID || UNITY_IOS)
    public static bool IsTapped()
    {
        return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
    }
#else
    public static bool IsTapped()
    {
        return Input.GetButtonDown("Fire1");
    }
#endif
}
