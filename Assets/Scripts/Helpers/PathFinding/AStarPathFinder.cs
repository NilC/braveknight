﻿//using UnityEngine;
//using System.Collections.Generic;

//public class AStarPathFinder
//{
//    public static TerrainPath GetPath(TerrainLogic terrainLogic, GridCoordinates startCoordinates, GridCoordinates targetCoordinates)
//    {
//        TerrainPath terrainPath = new TerrainPath();
        
//        List<AStarPathNode> openNodes = new List<AStarPathNode>();
//        List<AStarPathNode> closedNodes = new List<AStarPathNode>();
        
//        AStarPathNode startNode = new AStarPathNode()
//        {
//            Parent = null,
//            Coordinates = startCoordinates,
//            CostFromStart = 0,
//            CostToTarget = GetCost(startCoordinates, targetCoordinates)
//        };
        
//        AStarPathNode nearestNode = null;
//        openNodes.Add(startNode);
//        while (openNodes.Count > 0)
//        {
//            AStarPathNode currentNode = null;
//            float minTotalCost = float.MaxValue;
            
//            for (int i = 0; i < openNodes.Count; i++)
//            {
//                AStarPathNode openNode = openNodes [i];
//                if (openNode.TotalCost < minTotalCost)
//                {
//                    minTotalCost = openNode.TotalCost;
//                    currentNode = openNode;
//                    if (nearestNode == null || currentNode.CostToTarget < nearestNode.CostToTarget)
//                    {
//                        nearestNode = currentNode;
//                    }
//                }
//            }
            
//            if (currentNode.Coordinates == targetCoordinates)
//            {
//                terrainPath.Coordinates = GetPathForNode(currentNode);
//                terrainPath.Kind = LandscapePathKind.EndedAtTargetBlock;
//                return terrainPath;
//            }
            
//            closedNodes.Add(currentNode);
//            openNodes.Remove(currentNode);
            
//            List<AStarPathNode> neighbours = GetNeighbours(terrainLogic, currentNode, targetCoordinates);
//            foreach (AStarPathNode neighbourNode in neighbours)
//            {
//                bool isCurrentNodeInClosedNodes = false;
//                for (int i = closedNodes.Count - 1; i >= 0; i--)
//                {
//                    if (neighbourNode.Coordinates == closedNodes [i].Coordinates)
//                    {
//                        isCurrentNodeInClosedNodes = true;
//                        break;
//                    }
//                }
                
//                if (isCurrentNodeInClosedNodes)
//                {
//                    continue;
//                }
                
//                bool isCurrentNodeInOpenNodes = false;
//                for (int i = openNodes.Count - 1; i >= 0; i--)
//                {
//                    AStarPathNode openNode = openNodes [i];
//                    if (neighbourNode.Coordinates == openNode.Coordinates)
//                    {
//                        if (openNode.CostFromStart > neighbourNode.CostFromStart)
//                        {
//                            openNode.Parent = currentNode;
//                            openNode.CostFromStart = neighbourNode.CostFromStart;
//                        }
                        
//                        isCurrentNodeInOpenNodes = true;
//                        break;
//                    }
//                }
                
//                if (!isCurrentNodeInOpenNodes)
//                {
//                    openNodes.Add(neighbourNode);
//                }
//            }      
//        }
        
//        if (nearestNode != null)
//        {
//            terrainPath.Coordinates = GetPathForNode(nearestNode);
//            terrainPath.Kind = LandscapePathKind.EndedAtNearestBlock;
//        }
        
//        return terrainPath;
//    }
    
//    private static List<AStarPathNode> GetNeighbours(TerrainLogic terrainLogic, AStarPathNode pathNode, GridCoordinates targetPosition)
//    {
//        List<AStarPathNode> result = new List<AStarPathNode>();
        
//        GridCoordinates[] neighboursCoordinates = GetNeighboursCoordinates(pathNode.Coordinates);        
//        foreach (GridCoordinates neighbourCoordinates in neighboursCoordinates)
//        {
//            if (!terrainLogic.IsCoordinatesInVisiblePart(neighbourCoordinates))
//            {
//                continue;
//            }

//            GameObject block = terrainLogic.GetBlock(neighbourCoordinates);
//            if (block && block.GetComponent<BlockLogic>().PassableType != BlockPassableType.Backgound)
//            {
//                continue;
//            }
            
//            AStarPathNode neighbourNode = new AStarPathNode()
//            {
//                Parent = pathNode,
//                Coordinates = neighbourCoordinates,
//                CostFromStart = pathNode.CostFromStart + GetDistanceBetweenNeighbours(),
//                CostToTarget = GetCost(neighbourCoordinates, targetPosition)
//            };
//            result.Add(neighbourNode);
//        }
//        return result;
//    }
    
//    private static bool IsNeighbours(TerrainLogic terrainLogic, AStarPathNode pathNode, GridCoordinates possibleNeighborCoordinates)
//    {
//        GridCoordinates[] neighboursCoordinates = GetNeighboursCoordinates(pathNode.Coordinates);
//        foreach (GridCoordinates neighbourCoordinates in neighboursCoordinates)
//        {
//            if (!terrainLogic.IsCoordinatesInVisiblePart(neighbourCoordinates))
//            {
//                continue;
//            }
            
//            if (possibleNeighborCoordinates == neighbourCoordinates)
//            {
//                return true;
//            }
//        }
        
//        return false;
//    }
    
//    private static GridCoordinates[] GetNeighboursCoordinates(GridCoordinates coordinates)
//    {
//        // Соседними точками являются соседние по стороне клетки.
//        GridCoordinates[] neighboursCoordinates = new GridCoordinates[4];
//        neighboursCoordinates[0] = new GridCoordinates(coordinates.x, coordinates.y - 1);
//        neighboursCoordinates[1] = new GridCoordinates(coordinates.x, coordinates.y + 1);
//        neighboursCoordinates[2] = new GridCoordinates(coordinates.x + 1, coordinates.y);
//        neighboursCoordinates[3] = new GridCoordinates(coordinates.x - 1, coordinates.y);
//        return neighboursCoordinates;
//    }
    
//    private static int GetDistanceBetweenNeighbours()
//    {
//        return 1;
//    }
    
//    private static List<GridCoordinates> GetPathForNode(AStarPathNode pathNode)
//    {
//        List<GridCoordinates> path = new List<GridCoordinates>();
//        AStarPathNode currentNode = pathNode;
//        while (currentNode != null)
//        {
//            path.Add(currentNode.Coordinates);
//            currentNode = currentNode.Parent;
//        }
//        path.Reverse();
//        return path;
//    }
    
//    private static float GetCost(GridCoordinates startPosition, GridCoordinates targetPosition)
//    {
//        //return Mathf.Abs(targetPosition.x - startPosition.x) + Mathf.Abs(targetPosition.y - startPosition.y);
//        return startPosition.DistanceTo(targetPosition);
//    }
//}