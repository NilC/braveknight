﻿//using UnityEngine;
//using System.Collections.Generic;

//public class BreadthFirstPathFinder
//{
//    public static TerrainPath GetPath(TerrainLogic terrainLogic, GridCoordinates startCoordinates, GridCoordinates targetCoordinates)
//    {
//        TerrainPath terrainPath = new TerrainPath();

//        if (startCoordinates == targetCoordinates)
//        {
//            terrainPath.Kind = LandscapePathKind.EndedAtTargetBlock;
//            return terrainPath;
//        }

//        List<BreadthFirstPathNode> openNodes = new List<BreadthFirstPathNode>();
//        List<BreadthFirstPathNode> closedNodes = new List<BreadthFirstPathNode>();

//        BreadthFirstPathNode startNode = new BreadthFirstPathNode()
//        {
//            Parent = null,
//            Coordinates = startCoordinates,
//        };       

//        BreadthFirstPathNode targetNode = null;
//        BreadthFirstPathNode nearestNode = null;
//        float minDistance = float.MaxValue;
//        openNodes.Add(startNode); 
//        while (openNodes.Count > 0)
//        {        
//            BreadthFirstPathNode currentNode = openNodes[0];

//            GameObject currentBlock = terrainLogic.GetBlock(currentNode.Coordinates);
//            if (currentNode.Coordinates.DistanceTo(targetCoordinates) < minDistance &&
//                (!currentBlock || currentBlock.GetComponent<BlockLogic>().PassableType == BlockPassableType.Backgound))
//            {
//                minDistance = currentNode.Coordinates.DistanceTo(targetCoordinates);
//                nearestNode = currentNode;
//            }

//            List<BreadthFirstPathNode> neighbourNodes = GetNeighboursNodes(terrainLogic, currentNode);
//            foreach (BreadthFirstPathNode neighbourNode in neighbourNodes)
//            {
//                bool isCurrentNodeInClosedNodes = false;
//                for (int i = closedNodes.Count - 1; i >= 0; i--)
//                {
//                    if (neighbourNode.Coordinates == closedNodes [i].Coordinates)
//                    {
//                        isCurrentNodeInClosedNodes = true;
//                        break;
//                    }
//                }

//                if (isCurrentNodeInClosedNodes)
//                {
//                    continue;
//                }

//                GameObject block = terrainLogic.GetBlock(neighbourNode.Coordinates);

//                if (neighbourNode.Coordinates == targetCoordinates && 
//                    (!block || block.GetComponent<BlockLogic>().PassableType != BlockPassableType.Solid))
//                {
//                    targetNode = neighbourNode;
//                }

//                if (block && block.GetComponent<BlockLogic>().PassableType != BlockPassableType.Backgound)
//                {
//                    if (block.GetComponent<BlockLogic>().PassableType == BlockPassableType.Breakable)
//                    {
//                        closedNodes.Add(neighbourNode);
//                        terrainPath.AttainableBlocksCoordinates.Add(neighbourNode.Coordinates);
//                    }
//                    continue;
//                }

//                bool isNeighbourNodeInOpenNodes = false;
//                for (int i = openNodes.Count - 1; i >= 0; i--)
//                {
//                    if (neighbourNode.Coordinates == openNodes [i].Coordinates)
//                    {
//                        isNeighbourNodeInOpenNodes = true;
//                        break;
//                    }
//                }

//                if (!isNeighbourNodeInOpenNodes)
//                {
//                    openNodes.Add(neighbourNode);
//                }                
//            }

//            closedNodes.Add(currentNode);
//            openNodes.RemoveAt(0);
//        }

//        if (targetNode != null)
//        {
//            terrainPath.Coordinates = GetPathForNode(targetNode);
//            terrainPath.Kind = LandscapePathKind.EndedAtTargetBlock;
//        }
//        else if (nearestNode != null)
//        {
//            terrainPath.Coordinates = GetPathForNode(nearestNode);
//            terrainPath.Kind = LandscapePathKind.EndedAtNearestBlock;
//        }

//        return terrainPath;
//    }

//    private static List<GridCoordinates> GetPathForNode(BreadthFirstPathNode pathNode)
//    {
//        List<GridCoordinates> path = new List<GridCoordinates>();
//        BreadthFirstPathNode currentNode = pathNode;
//        while (currentNode != null)
//        {
//            path.Add(currentNode.Coordinates);
//            currentNode = currentNode.Parent;
//        }
//        path.Reverse();
//        return path;
//    }
    
//    private static List<BreadthFirstPathNode> GetNeighboursNodes(TerrainLogic terrainLogic, BreadthFirstPathNode pathNode)
//    {
//        List<BreadthFirstPathNode> result = new List<BreadthFirstPathNode>();
        
//        GridCoordinates[] neighboursCoordinates = GetNeighboursCoordinates(pathNode.Coordinates);    
//        foreach (GridCoordinates neighbourCoordinates in neighboursCoordinates)
//        {
//            if (!terrainLogic.IsCoordinatesInCreatedPart(neighbourCoordinates))
//            {
//                continue;
//            }

//            BreadthFirstPathNode neighbourNode = new BreadthFirstPathNode()
//            {
//                Parent = pathNode,
//                Coordinates = neighbourCoordinates,
//            };
//            result.Add(neighbourNode);
//        }
//        return result;
//    }   
    
//    private static GridCoordinates[] GetNeighboursCoordinates(GridCoordinates coordinates)
//    {
//        // Соседними точками являются соседние по стороне клетки.
//        GridCoordinates[] neighboursCoordinates = new GridCoordinates[4];
//        neighboursCoordinates[0] = new GridCoordinates(coordinates.x, coordinates.y - 1);
//        neighboursCoordinates[1] = new GridCoordinates(coordinates.x, coordinates.y + 1);
//        neighboursCoordinates[2] = new GridCoordinates(coordinates.x + 1, coordinates.y);
//        neighboursCoordinates[3] = new GridCoordinates(coordinates.x - 1, coordinates.y);
//        return neighboursCoordinates;
//    }
//}
