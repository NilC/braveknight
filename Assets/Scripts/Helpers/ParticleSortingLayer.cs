﻿using UnityEngine;
using System.Collections;

public class ParticleSortingLayer : MonoBehaviour
{
    public string SortingLayerName;
    public int SortingOrder;

    void Start()
    {
        particleSystem.renderer.sortingLayerName = SortingLayerName;
        particleSystem.renderer.sortingOrder = SortingOrder;
    }
}
