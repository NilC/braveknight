﻿using UnityEngine;

public class MathHelper : MonoBehaviour
{
    public static int WeightedRandomIndex(float[] weights)
    {
        float sumOfWeights = 0.0f;
        for (int i = 0; i < weights.Length; i++)
        {
            sumOfWeights += weights[i];
        }

        float random = Random.Range(0.0f, sumOfWeights);
        for (int i = 0; i < weights.Length; i++)
        {
            if (random < weights[i])
            {
                return i;
            }
            random -= weights[i];
        }

        throw new UnityException("Should never get here");
    }

    public static void ReverseXScale(Transform transform)
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    public static bool RandomFlag()
    {
        return (Random.Range(0, 100) < 50);
    }
}
