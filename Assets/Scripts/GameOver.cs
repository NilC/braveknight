﻿using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public Text LastWords;

    private void Update()
    {
        LastWords.text = string.Format("ПОТРАЧЕНО ПРИНЦЕСС: {0}; ПРОЙДЕНО {1} м",
            Dependences.PrincessSpawner.Ammount - Dependences.Score.ScoreAmmount, Dependences.Score.DepthScore);
    }
}
