﻿using UnityEngine;

public class Hero : MonoBehaviour
{
    public LayerMask WallsMask;

    public float HorizontalSpeed = 1.0f;
    public float FrictionFactor = 0.98f;
    public float JumpVelocity = 10.0f;
    public float AirFriction = 0.999f;

    public GameObject Sparks;

    public float Speed;
    //public bool IsWallCollided;

//    private bool IsWallCollided;

    private HeroStatesManager statesManager;

    public void Start()
    {
        statesManager = new HeroStatesManager(this, HeroState.Clamb);
    }

    public void Update()
    {
        Speed = rigidbody2D.velocity.y;

        statesManager.UpateStates();
        Dependences.DebugLabel.text = string.Format("Hero state: {0}", statesManager.State);
    }

    public void FixedUpdate()
    {
        statesManager.CurrentState.CustomFixedUpdate();
    }

    public void Jump()
    {
        statesManager.CurrentState.Jump();
    }

    public bool IsWallCollided()
    {
        return Physics2D.OverlapArea(collider2D.bounds.min - new Vector3(0.1f, 0.1f),
             collider2D.bounds.max + new Vector3(0.1f, 0.1f), WallsMask);
    }

    public bool IsDead()
    {
        return statesManager.State == HeroState.Falling ||
               statesManager.State == HeroState.Smash;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!IsDead() && other.gameObject.tag == "Princess")
        {
            Destroy(other.gameObject);
            Dependences.Score.ScoreAmmount += 1;
            Dependences.AudioManager.PlayPickUpSound(transform.position);
        }
        //Velocity = new Vector2(-Velocity.x, Velocity.y);
    }
}
