﻿
using UnityEngine;

public class HeroPreFallingState : HeroStateBase
{
    public float FallingTime;
    private float currentFallingTime;

    public override void OnEnter(HeroState previousState)
    {
        base.OnEnter(previousState);
        TriggerAnimation(Triggers.PreFalling);
        currentFallingTime = 0.0f;
    }

    public override void CustomFixedUpdate()
    {
        base.CustomFixedUpdate();
        currentFallingTime += Time.deltaTime;
        if (currentFallingTime > FallingTime)
        {
            StatesManager.ChangeState(HeroState.Falling);
        }
        else if (Hero.IsWallCollided())
        {
            StatesManager.ChangeState(HeroState.Clamb);
        }
    }
}

