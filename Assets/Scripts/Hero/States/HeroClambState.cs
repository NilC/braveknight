﻿using UnityEngine;

public class HeroClambState : HeroStateBase
{
    public override void OnEnter(HeroState previousState)
    {
        base.OnEnter(previousState);
        TriggerAnimation(Triggers.Clamp);
        if (previousState == HeroState.Jump || previousState == HeroState.PreFalling)
        {
            Dependences.AudioManager.PlayClampSound(transform.position);    
        }        
    }

    public override void CustomFixedUpdate()
    {
        base.CustomFixedUpdate();
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, rigidbody2D.velocity.y * Hero.FrictionFactor);
    }

    public override void Jump()
    {
        if (Hero.IsWallCollided())
        {
            //Dependences.AudioManager.PlayJumpSound(transform.position);
            Hero.HorizontalSpeed = -Hero.HorizontalSpeed;
            MathHelper.ReverseXScale(transform);
            Hero.rigidbody2D.velocity = new Vector2(Hero.HorizontalSpeed, rigidbody2D.velocity.y + Hero.JumpVelocity);
            StatesManager.ChangeState(HeroState.Jump);
        }
    }
}

