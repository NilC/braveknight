﻿using UnityEngine;

public class HeroJumpState : HeroStateBase
{
    private bool isWasInAir;

    public override void OnEnter(HeroState previousState)
    {
        base.OnEnter(previousState);
        TriggerAnimation(Triggers.Jump);
        isWasInAir = false;       
    }

    public override void CustomFixedUpdate()
    {
        base.CustomFixedUpdate();
        bool isWallCollided = Hero.IsWallCollided();

        if (isWallCollided && isWasInAir)
        {            
            StatesManager.ChangeState(HeroState.Clamb);
        }
        else
        {
            if (isWallCollided == false)
            {
                isWasInAir = true;
            }

            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, rigidbody2D.velocity.y * Hero.AirFriction);
        }
    }
}
