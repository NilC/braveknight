﻿using UnityEngine;

public class HeroSmashState : HeroStateBase
{
    public override void OnEnter(HeroState previousState)
    {
        IsLocked = true;
        base.OnEnter(previousState);
        TriggerAnimation(Triggers.Smash);
        rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
        Dependences.AudioManager.PlaySmashSound(transform.position);
    }
}
