﻿using UnityEngine;

public class HeroStateBase : MonoBehaviour
{
    public const float StayingTime = 0.08f;
    public bool IsLocked { get; protected set; }

    protected HeroStatesManager StatesManager;
    protected Hero Hero;

    private Animator animator;
    private float currentStayingTime;

    public void Initialize(HeroStatesManager statesManager, Hero hero)
    {
        StatesManager = statesManager;
        Hero = hero;
        animator = Hero.GetComponentInChildren<Animator>();
    }

    public virtual void OnEnter(HeroState previousState)
    {
        currentStayingTime = 0.0f;
    }

    public virtual void OnLeave()
    {
    }

    public virtual void CustomUpdate()
    {
        if (StatesManager.State != HeroState.PreFalling &&
            Mathf.Abs(rigidbody2D.velocity.x) < 0.01f && !Hero.IsWallCollided())
        {
            StatesManager.ChangeState(HeroState.PreFalling);
        }

        if (Mathf.Abs(rigidbody2D.velocity.y) < 0.01f)
        {
            currentStayingTime += Time.deltaTime;
            if (currentStayingTime > StayingTime)
            {
                StatesManager.ChangeState(HeroState.Smash);
            }
        }
        else
        {
            currentStayingTime = 0.0f;    
        }
    }

    public virtual void CustomFixedUpdate()
    {
    }

    public virtual void Jump()
    {
    }

    protected void TriggerAnimation(string triggerName)
    {
        animator.SetTrigger(triggerName);
    }
}
