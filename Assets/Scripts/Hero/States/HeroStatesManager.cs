﻿
public class HeroStatesManager
{
    private readonly HeroStateBase[] states;

    public HeroStateBase CurrentState { private set; get; }
    public HeroState State { private set; get; }
    private HeroState? nextState;

    public HeroStatesManager(Hero hero, HeroState initializeState)
    {
        states = new HeroStateBase[]
        {
            //Необходимо, чтобы индексы совпадали с enum HeroState
            hero.GetComponent<HeroClambState>(),
            hero.GetComponent<HeroJumpState>(),
            hero.GetComponent<HeroPreFallingState>(),
            hero.GetComponent<HeroFallingState>(),
            hero.GetComponent<HeroSmashState>()
        };

        for (int i = 0; i < states.Length; i++)
        {
            states[i].Initialize(this, hero);
        }

        InitializeWithState(initializeState);
    }

    public void UpateStates()
    {
        CurrentState.CustomUpdate();

        if (!nextState.HasValue || CurrentState.IsLocked)
        {
            return;
        }

        CurrentState.OnLeave();
        HeroState previousState = State;
        State = nextState.Value;
        nextState = null;
        CurrentState = states[(int)State];
        CurrentState.OnEnter(previousState);
    }

    public void ChangeState(HeroState heroState)
    {
        if (heroState != State)
        {
            nextState = heroState;    
        }        
    }

    private void InitializeWithState(HeroState state)
    {
        State = state;
        states[(int)State].OnEnter(HeroState.Clamb);
        CurrentState = states[(int)State];
    }
}

