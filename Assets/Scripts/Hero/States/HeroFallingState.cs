﻿using UnityEngine;

public class HeroFallingState : HeroStateBase
{
    public override void OnEnter(HeroState previousState)
    {       
        IsLocked = true;
        base.OnEnter(previousState);
        TriggerAnimation(Triggers.Falling);
    }
    
    public override void CustomFixedUpdate()
    {
        base.CustomFixedUpdate();
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, rigidbody2D.velocity.y * Hero.AirFriction);
    }
}
