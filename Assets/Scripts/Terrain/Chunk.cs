﻿using UnityEngine;

public class Chunk : MonoBehaviour
{
    private bool isTriggeredByCreator;

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case Tags.Creator:
                if (!isTriggeredByCreator)
                {
                    Dependences.TerrainGenerator.CreateNewChunk(transform.position.y);
                    isTriggeredByCreator = true;
                }
                break;

            case Tags.Terminator:
                Dependences.TerrainGenerator.DestroyChunk(gameObject);
                break;
        }
    }
}
