﻿using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public GameObject[] Chunks;

    public float ChunkX;
    public float ChunkHeight;

    private int previousIndex = -1;

    public void CreateNewChunk(float previousPositionY)
    {
        int randomIndex = 0;
        if (Chunks.Length > 1)
        {
            randomIndex = previousIndex;
            while (randomIndex == previousIndex)
            {
                randomIndex = Random.Range(0, Chunks.Length);
            }
        }

        Instantiate(Chunks[randomIndex], new Vector3(ChunkX, previousPositionY - ChunkHeight, 0.0f), Quaternion.identity);
    }

    public void DestroyChunk(GameObject chunk)
    {
        Destroy(chunk);
    }
}
