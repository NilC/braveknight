﻿using UnityEngine;
using System.Collections;

public class Scroller : MonoBehaviour
{
    public Transform CameraTransform;
    public float Ratio = 1;

    private GameObject top;
    private GameObject down;
    private float backgroundSize;
    private float startingPosition;
    private float cameraStartingPosition;

    //private Vector2 off;
    // Use this for initialization
    void Start()
    {
        top = transform.GetChild(1).gameObject;
        down = transform.GetChild(0).gameObject;
        startingPosition = transform.position.y;
        cameraStartingPosition = CameraTransform.position.y;

        var downRenderer = down.GetComponent<SpriteRenderer>();
        backgroundSize = downRenderer.sprite.bounds.size.y;

        down.transform.localPosition = new Vector3(0, backgroundSize, 0);
    }

    // Update is called once per frame
    void Update()
    {
        var ammount = -CameraTransform.position.y + cameraStartingPosition;
        transform.position = new Vector3(transform.position.x, -ammount + startingPosition, transform.position.z);
        ammount = Mathf.Repeat(ammount * Ratio, backgroundSize);

        top.transform.localPosition = Offset(top.transform.localPosition, ammount);
        down.transform.localPosition = Offset(down.transform.localPosition, ammount - backgroundSize);
    }

    private Vector3 Offset(Vector3 origin, float value)
    {
        return new Vector3(origin.x, value, origin.z);
    }
}
